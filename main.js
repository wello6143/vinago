const electron = require('electron')
const {app, BrowserWindow} = require('electron')
  let win
  const Menu = electron.Menu;
  function createWindow () {
    win = new BrowserWindow({width: 1024, height: 768, icon: __dirname +  './img/logo.png'})
    const menuTemplate = [
        {
            label: 'Tùy chọn',
            submenu: [
                {
                    label: 'Sao chép',
                    role: 'copy'
                },
                {
                    label: 'Cắt',
                    role: 'cut'
                },
                {
                    label: 'Dán',
                    role: 'paste'
                },
                {
                    label: 'Chọn tất cả',
                    role: 'selectAll'
                },
                {
                    label: 'Về VinaGo',
                    click(){
                        openAboutWindow();
                    },
                },
                {
                    label: 'Thoát',
                    role: 'quit'
                },
              ],
        },
        {
            label: 'Quay về trang chủ',
            click(){
            }
        },
        {
            label: 'Tải lại',
            role: 'reload'
        },
        {
            label: 'Công cụ cho nhà phát triển',
            role: 'toggleDevTools'
        },
        {
            label: 'Toàn màn hình',
            role: 'toggleFullScreen'
        }
    ];
     let newWindow

     function openAboutWindow() {
      if (newWindow) {
       newWindow.focus()
       return;
     }

     newWindow = new BrowserWindow({
      height: 600,
      width: 600,
      title: "Về VinaGo",
      minimizable: false,
      maximizable: false,
      fullscreenable: false
     });
     newWindow.setMenu(null);
     newWindow.loadURL(__dirname + './about.html');

      newWindow.on('closed', function () {
      newWindow = null;
     });
    };
    const menu = Menu.buildFromTemplate(menuTemplate);
    Menu.setApplicationMenu(menu);
    win.loadFile('index.html');
    win.on('closed', () => {
      win = null
    })
  }
  
  
  app.on('ready', createWindow)
  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
      app.quit()
    }
  })
  app.on('activate', () => {
    if (win === null) {
      createWindow()
    }
  })
